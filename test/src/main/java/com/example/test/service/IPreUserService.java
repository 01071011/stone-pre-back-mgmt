package com.example.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.test.bean.PreUser;

public interface IPreUserService extends IService<PreUser> {

    PreUser login(PreUser preUser);

    PreUser register(PreUser preUser);

    PreUser changePass(PreUser preUser);
}
