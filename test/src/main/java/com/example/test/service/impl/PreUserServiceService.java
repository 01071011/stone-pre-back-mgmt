package com.example.test.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.test.bean.PreUser;
import com.example.test.mapper.PreUserMapper;
import com.example.test.service.IPreUserService;
import org.springframework.stereotype.Service;

@Service
public class PreUserServiceService extends ServiceImpl<PreUserMapper,PreUser> implements IPreUserService {

    @Override
    public PreUser login(PreUser preUser) {
        LambdaQueryWrapper<PreUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(PreUser::getAccount,preUser.getAccount())
        .eq(PreUser::getPassword,preUser.getPassword());
        PreUser res = baseMapper.selectOne(lambdaQueryWrapper);
        if (ObjectUtils.isEmpty(res)) {
            preUser.setLoginFlag(false);
            preUser.setErrorMes("账号密码错误。");
        }else
            preUser.setLoginFlag(true);
        return preUser;
    }

    @Override
    public PreUser register(PreUser preUser) {
        if (!preUser.getPassword().equals(preUser.getConPassword())) {
            preUser.setLoginFlag(false);
            preUser.setErrorMes("两次密码输入不一致。");
            return preUser;
        }
        LambdaQueryWrapper<PreUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(PreUser::getAccount,preUser.getAccount());
        PreUser res = baseMapper.selectOne(lambdaQueryWrapper);
        if (ObjectUtils.isEmpty(res)){
            preUser.setLoginFlag(true);
            baseMapper.insert(preUser);
        } else{
            preUser.setLoginFlag(false);
            preUser.setErrorMes("账号已存在。");
        }
        return preUser;
    }

    @Override
    public PreUser changePass(PreUser preUser) {
        if (!preUser.getPassword().equals(preUser.getConPassword())){
            preUser.setLoginFlag(false);
            preUser.setErrorMes("两次密码输入不一致。");
            return preUser;
        }
        LambdaQueryWrapper<PreUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(PreUser::getPhone,preUser.getPhone())
                .eq(PreUser::getAccount,preUser.getAccount())
                .eq(PreUser::getPassword,preUser.getOldPassword());
        PreUser selectOne = baseMapper.selectOne(lambdaQueryWrapper);
        if (ObjectUtils.isEmpty(selectOne)){
            preUser.setLoginFlag(false);
            preUser.setErrorMes("账号密码错误，无法修改");
        }else{
            selectOne.setPassword(preUser.getPassword());
            baseMapper.updateById(selectOne);
            preUser.setLoginFlag(true);
        }
        return preUser;
    }


}
