package com.example.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.test.bean.PreArticle;

public interface IPreArticleService  extends IService<PreArticle> {
    int saveArticle(PreArticle preArticle);
}
