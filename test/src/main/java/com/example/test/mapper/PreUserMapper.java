package com.example.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.test.bean.PreUser;

public interface PreUserMapper extends BaseMapper<PreUser> {


    int selfInsert(PreUser preUser);
}
