package com.example.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.test.bean.PreArticle;

public interface PreArticleMapper extends BaseMapper<PreArticle> {
}
