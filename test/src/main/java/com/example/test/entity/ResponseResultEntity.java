package com.example.test.entity;

import lombok.Data;

/**
 * 〈一句话功能简述〉<br>
 * 〈数据返回格式封装〉
 *
 * @author lyp
 * @date 2019/10/21
 */
@Data
public class ResponseResultEntity<T> {

    public ResponseResultEntity(Integer code, T data, String msg){
        this.code = code;
        this.data = data;
        this.msg = msg;
    }
    /**
     * 返回状态码
     */
    private Integer code;

    /**
     * 返回内容数据
     */
    private T data;

    /**
     * 返回数据描述信息
     */
    private String msg;
}

