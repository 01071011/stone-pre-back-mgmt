package com.example.test.controller;

import com.example.test.bean.PreUser;
import com.example.test.entity.ResponseResultEntity;
import com.example.test.service.impl.PreUserServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("loginController")
public class PreUserController {

    @Autowired
    private PreUserServiceService preUserService;


    @PostMapping("/login")
    public ResponseResultEntity<?> login(@RequestBody() PreUser preUser){
        PreUser resPreUser = preUserService.login(preUser);
        return new ResponseResultEntity<>(HttpStatus.OK.value(), resPreUser, HttpStatus.OK.getReasonPhrase());
    }

    @PostMapping("/register")
    public ResponseResultEntity<?> register(@RequestBody PreUser preUser){
        PreUser resPreUser = preUserService.register(preUser);
        return new ResponseResultEntity<>(HttpStatus.OK.value(), resPreUser, HttpStatus.OK.getReasonPhrase());
    }

    @PostMapping("/changePass")
    public ResponseResultEntity<?> changePass(@RequestBody PreUser preUser){
        PreUser resPreUser = preUserService.changePass(preUser);
        return new ResponseResultEntity<>(HttpStatus.OK.value(), resPreUser, HttpStatus.OK.getReasonPhrase());
    }


}
