package com.example.test.controller;

import com.example.test.bean.PreArticle;
import com.example.test.entity.ResponseResultEntity;
import com.example.test.service.impl.PreArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("article")
public class PreArcticController {

    @Autowired
    private PreArticleService preArticleService;

    @PostMapping("/save")
    public ResponseResultEntity<?> save(@RequestBody() PreArticle preArticle){
        return new ResponseResultEntity<>(HttpStatus.OK.value(), preArticleService.saveArticle(preArticle), HttpStatus.OK.getReasonPhrase());
    }
}
