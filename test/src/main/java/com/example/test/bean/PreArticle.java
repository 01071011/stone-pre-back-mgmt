package com.example.test.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.List;

@Data
public class PreArticle {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String user_name;
    private String tab;
    private String title;
    private String description;
    private String article;
    private String user_id;

    @TableField(exist = false)
    private List<String> tabs;

}
