package com.example.test.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class PreUser{
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String account;
    private String  password;
    private String phone;
    private String name;

    @TableField(exist = false)
    private boolean loginFlag;

    @TableField(exist = false)
    private String conPassword;

    @TableField(exist = false)
    private String errorMes;

    @TableField(exist = false)
    private String oldPassword;

}
